package com.bitsmi.yggdrasil.commons.nativetest;

import static org.assertj.core.api.Assertions.assertThat;

import com.bitsmi.yggdrasil.commons.util.PropertiesEvaluator;

public class MainProgram
{
    public static void main(String... args)
    {
        try {
            MainProgram instance = new MainProgram();        
            instance.testPropertiesEvaluator();
        }
        catch(Exception e) {
            System.err.println("ERROR: " + e.getMessage());
            e.printStackTrace();
        }
    }
    
    private void testPropertiesEvaluator() throws Exception
    {
        System.out.println("TEST testPropertiesEvaluator STARTED");
        
        // URL path properties
        PropertiesEvaluator evaluator = new PropertiesEvaluator();
        evaluator.addFilePropertiesSourceFromClasspath("/com/bitsmi/yggdrasil/commons/nativetest/evaluator.properties");
        
        PojoWithValues obj = new PojoWithValues();
        evaluator.injectValues(obj);
        
        System.out.println("VALUES: " + obj.toString());
        
        assertThat(obj.getParam1()).isEqualTo("key4");
        assertThat(obj.getParam2()).isEqualTo("value2");
        assertThat(obj.getParam3()).isEqualTo("value5");
        assertThat(obj.getParam4()).isEqualTo("value4");
        assertThat(obj.getParam5()).isEqualTo("value5");
        assertThat(obj.getParam6()).isEqualTo("value6");
        assertThat(obj.getParam7()).isEqualTo("value7");
        assertThat(obj.getParam8()).isEqualTo("value8");
        assertThat(obj.getParam9()).isEqualTo("value9");
        
        System.out.println("TEST testPropertiesEvaluator FINISHED");
    }
}
