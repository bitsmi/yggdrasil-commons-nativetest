module com.bitsmi.yggdrasil.commons.nativetest
{
    requires com.bitsmi.yggdrasil.commons;
    requires org.assertj.core;
    
    opens com.bitsmi.yggdrasil.commons.nativetest;
}