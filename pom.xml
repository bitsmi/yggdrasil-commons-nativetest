	<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0
                             http://maven.apache.org/maven-v4_0_0.xsd">
    <modelVersion>4.0.0</modelVersion>
	
	<groupId>com.bitsmi.yggdrasil</groupId>
    <artifactId>yggdrasil-commons-nativetest</artifactId>
	<version>1.0.0-SNAPSHOT</version>
    <packaging>jar</packaging>
	<name>Yggdrasil Platform Commons Native Test</name>
	
	<properties>
		<java.version>11</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.resources.sourceEncoding>UTF-8</project.resources.sourceEncoding>
		
		<junit.jupiter.version>5.7.2</junit.jupiter.version>
		<junit.vintage.version>${junit.jupiter.version}</junit.vintage.version>
		<junit.platform.version>1.7.2</junit.platform.version>
		
		<!-- Properties for mvn exec:java -->
    	<mainClass>com.bitsmi.yggdrasil.commons.nativetest.MainProgram</mainClass>
	</properties>

	<dependencies>
		<dependency>
			<groupId>com.bitsmi.yggdrasil</groupId>
			<artifactId>yggdrasil-commons</artifactId>
			<version>2.0.0-SNAPSHOT</version>
		</dependency>
	
		<!-- TEST -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-api</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-params</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>		
		<!-- Only required to run tests in an IDE that bundles an older version -->
		<dependency>
			<groupId>org.junit.platform</groupId>
			<artifactId>junit-platform-launcher</artifactId>
			<version>${junit.platform.version}</version>
			<scope>test</scope>
		</dependency>
		<!-- Only required to run tests in an IDE that bundles an older version -->
		<dependency>
			<groupId>org.junit.jupiter</groupId>
			<artifactId>junit-jupiter-engine</artifactId>
			<version>${junit.jupiter.version}</version>
			<scope>test</scope>
		</dependency>
		<!-- Only required to run tests in an IDE that bundles an older version -->
		<dependency>
			<groupId>org.junit.vintage</groupId>
			<artifactId>junit-vintage-engine</artifactId>
			<version>${junit.vintage.version}</version>
			<scope>test</scope>
		</dependency>
		<!-- To avoid compiler warnings about @API annotations in JUnit code -->
		<dependency>
			<groupId>org.apiguardian</groupId>
			<artifactId>apiguardian-api</artifactId>
			<version>1.0.0</version>
			<scope>test</scope>
		</dependency>
		<!-- Allow usage in MainProgram using compile scope -->
		<dependency>
			<groupId>org.assertj</groupId>
			<artifactId>assertj-core</artifactId>
			<version>3.20.2</version>
		</dependency>
		<!-- Won't be needed as of JUnit 5.8 -->
		<dependency>
    		<groupId>org.graalvm.buildtools</groupId>
    		<artifactId>junit-platform-native</artifactId>
    		<version>0.9.4</version>
    		<scope>test</scope>
		</dependency>		
	</dependencies>
	
	<build>
		<resources>
            <resource>
              	<directory>${project.basedir}/src/main/resources</directory>
              	<filtering>true</filtering>
              	<!-- Exclude fonts files because resulting of filtering may be corrupted -->
              	<excludes>
					<exclude>static/fonts/**</exclude>
        		</excludes>
            </resource>
            <!-- Include previously excluded files without filtering -->
            <resource>
        		<directory>${project.basedir}/src/main/resources</directory>
        		<filtering>false</filtering>
        		<includes>
            		<include>static/fonts/**</include>
        		</includes>
    		</resource>
        </resources>
        <testResources>
			<testResource>
				<directory>${project.basedir}/src/test/resources</directory>								
				<filtering>true</filtering>
			</testResource>
		</testResources>
	
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.8.0</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
					<encoding>${project.build.sourceEncoding}</encoding>
					<sourceEncoding>${project.build.sourceEncoding}</sourceEncoding>
				</configuration>
			</plugin>
			
			<plugin>
				<artifactId>maven-surefire-plugin</artifactId>
				<version>3.0.0-M5</version>
				<configuration>
					<includes>
						<include>**/Test*.java</include>
						<include>**/*Test.java</include>
						<include>**/*Tests.java</include>
						<include>**/*TestCase.java</include>
					</includes>
					<!-- Uncomment to generate native analysis files -->			
					<!-- Once finished, replace files in src/test/resources/META-INF/native-image -->
					<!-- <argLine>-agentlib:native-image-agent=config-merge-dir=analysis-output</argLine> -->
				</configuration>
			</plugin>
			
			<!-- Run mvn jacoco:report to generate HTML report on target/site folder-->
			<plugin>
			    <groupId>org.jacoco</groupId>
			    <artifactId>jacoco-maven-plugin</artifactId>
			    <version>0.8.5</version>
			    <executions>
			        <execution>
			            <goals>
			                <goal>prepare-agent</goal>
			            </goals>
			        </execution>
			        <execution>
			            <id>report</id>
			            <phase>prepare-package</phase>
			            <goals>
			                <goal>report</goal>
			            </goals>
			        </execution>
			    </executions>
			</plugin>
			
			<!-- Execute application with mvn clean compile exec:exec -->
			<plugin>
			    <groupId>org.codehaus.mojo</groupId>
			    <artifactId>exec-maven-plugin</artifactId>
			    <version>3.0.0</version>
			    <configuration>
			    	<executable>java</executable>
         			<arguments>
           				<argument>--module-path</argument>
           				<!-- automatically creates the modulepath using all project dependencies, also adding the project build directory -->
           				<modulepath/>
           				<!-- Uncomment to generate native analysis files -->			
						<!-- Once finished, replace files in src/test/resources/META-INF/native-image -->
						<!-- <argument>-agentlib:native-image-agent=config-merge-dir=analysis-output</argument> -->
						<argument>--module</argument>
           				<argument>com.bitsmi.yggdrasil.commons.nativetest/${mainClass}</argument>
         			</arguments>			    
			    </configuration>
			</plugin>
		</plugins>			
	</build>
	
	<profiles>
		<profile>
			<id>java11-graalvm</id>
			<build>
				<plugins>
					<plugin>					
						<artifactId>maven-toolchains-plugin</artifactId>					
						<executions>
						  <execution>
							<goals>
							  <goal>toolchain</goal>
							</goals>
						  </execution>
						</executions>
						<configuration>
						  <toolchains>
							<jdk>
							  <version>11</version>
							  <vendor>GraalVM</vendor>
							</jdk>
						  </toolchains>
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
		
		<profile>
			<id>native</id>
			<build>
				<plugins>
					<plugin>
						<groupId>org.graalvm.buildtools</groupId>
						<artifactId>native-maven-plugin</artifactId>
						<version>0.9.4</version>
						<executions>
							<execution>
								<id>build-native</id>
								<goals>
									<goal>build</goal>
								</goals>
								<phase>package</phase>
							</execution>
							<execution>
								<id>test-native</id>
								<goals>
									<goal>test</goal>
								</goals>
								<phase>test</phase>								
							</execution>
						</executions>
						<configuration>
							<mainClass>${mainClass}</mainClass>
							<buildArgs>
								<buildArg>--verbose</buildArg>
								<buildArg>--no-fallback</buildArg>
								<!-- Needed for JUnit in Native test -->
								<buildArg>--allow-incomplete-classpath</buildArg>
								<!-- List included resources -->
								<buildArg>-H:Log=registerResource</buildArg>
							</buildArgs>							
						</configuration>
					</plugin>
				</plugins>
			</build>
		</profile>
	</profiles>

</project>